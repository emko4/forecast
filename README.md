Forecast
=================
 
## Development (localhost)

* Install packages by [Yarn](https://yarnpkg.com/en/)
    * `yarn`
* Set *NODE_ENV* to *development*
* Run npm script start by [Yarn](https://yarnpkg.com/en/) 
    * `yarn start`
* Dev server is running on http://localhost:8080    

## Production

* Install packages by [Yarn](https://yarnpkg.com/en/)
    * `yarn`
* Set *NODE_ENV* to *production*
* Run npm script build by [Yarn](https://yarnpkg.com/en/) 
    * `yarn build`
* Production files are in *dist* folder