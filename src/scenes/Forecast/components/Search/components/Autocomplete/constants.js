import { REQUEST } from '../../../../../../services/api/constants';

export const LOCALE_PREFIX = 'scenes.Forecast.components.Search.components.Autocomplete';

export const SEARCH_REQUEST = `SEARCH_${REQUEST}`;
export const SELECT_ITEM = 'SELECT_ITEM';
export const RESET_SEARCH = 'RESET_SEARCH';
