import { REQUEST } from '../../../../../../services/api/constants';

export const LOCALE_PREFIX = 'scenes.Forecast.components.Search.components.Weather';

export const FORECAST_REQUEST = `FORECAST_${REQUEST}`;
