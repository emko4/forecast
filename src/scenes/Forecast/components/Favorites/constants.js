export const LOCALE_PREFIX = 'scenes.Forecast.components.Favorites';

export const ADD_TO_FAVORITES = 'ADD_TO_FAVORITES';
export const REMOVE_FROM_FAVORITES = 'REMOVE_FROM_FAVORITES';
